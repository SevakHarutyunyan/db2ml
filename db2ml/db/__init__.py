import os
from configparser import ConfigParser

from pymongo import MongoClient

DIR = os.path.dirname(__file__)
DB = os.path.join(DIR, "DB.ini")

config = ConfigParser()
config.read(DB)
client = MongoClient()

db_name = config.sections()[0]

db = client[config[db_name]["name"]]

DOCUMENTS = db[config[db_name]["documents"]]
COLLECTIONS = db[config[db_name]["collections"]]
TEMPLATES = db[config[db_name]["templates"]]
