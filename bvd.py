import json
import os

from db2ml.models.collection import Collection
from db2ml.parser import DocParser

############################
COLLECTION_NAME = "BVD"
TEMPLATE_NAMES = [
            "BVD Assets cash and loans", 
            "BVD Assets FA", 
            "BVD Assets (In)tangibles and other"
]

DATA_DIR = __file__.split(".")[:-1][0] + "_data"

try:
    os.mkdir(DATA_DIR)
except OSError as err:
    pass
#############################

collection = Collection(COLLECTION_NAME)
for document in collection.documents:
    data = {"output": []}
    for template in document.templates:
        if template.name is None or template.name not in TEMPLATE_NAMES:
            continue
        for field in template.fields:
            for tag in field.tags:
                if tag.value:    
                    parser = DocParser(document=document, template=template, field=field, tag=tag)
                    data["output"].append(parser.to_json())
    if not data["output"]:
        continue
    with open(os.path.join(DATA_DIR, document.name.replace(".pdf", "") + ".json",), "w") as fp:
        json.dump(data, fp, indent=4)
