from db2ml.models.collection import Collection
from db2ml.models.document import Document
from db2ml.models.template import Template
from db2ml.models.field import Field
from db2ml.models.tag import Tag

class DocParser:
    """Document Parser"""

    def __init__(self,
                collection: Collection=None, 
                document: Document=None, 
                template: Template=None, 
                field: Field=None, 
                tag: Tag=None
        ):
        self.document = document
        self.template = template
        self.field = field
        self.tag = tag
        self.make()

    def make(self):
        self.__set_document_info()
        self.__set_template_info()
        self.__set_field_info()
        self.__set_tag_info()

    def __set_document_info(self):
        self.doc_name = self.document.name
        self.doc_src = self.document.src

    def __set_template_info(self):
        self.template_name = self.template.name

    def __set_field_info(self):
        self.group = self.field.group
        self.field_name = self.field.name

    def __set_tag_info(self):
        self.value = self.tag.value
        self.top = self.tag.top
        self.left = self.tag.left
        self.width = self.tag.width
        self.height = self.tag.height
        self.page = self.tag.page


    def to_json(self, full: bool=False):
        self.__json = {}
        self.__json["template"] = self.template_name
        self.__json["group"] = self.group
        self.__json["field"] = self.field_name
        self.__json["value"] = self.value
        self.__json["top"] = self.top
        self.__json["left"] = self.left
        self.__json["width"] = self.width
        self.__json["height"] = self.height
        self.__json["page"] = self.page
        if full:
            self.__json["doc"] = self.doc_name
            self.__json["src"] = self.doc_src
        return self.__json


    def to_csv(self):
        raise NotImplementedError