# db2ml

db2ml is a pythonic wrapper for querying the database for machine leaning tasks.

It cannot do complex queries yet, but provides a nice API for getting all the data you need for ML models.


## Setup

1. **Add Innolytiq dump to your mongo database**

`mongorestore -d innolytiq-prod <path-to-dump>/`

```
    You can change the name of db whatever you want,
    after `-d` flag,  but also change it in `db2ml/db/DB.ini` file. 
    By default it is `innolytiq-prod`
```
	
2. **Start mongo session**

    `mongo`

3. **Setup python environment**

	`python3 -m venv .venv`

	`source .venv/bin/activate`

	`pip install --upgrade pip`

	`pip install -r requirements.txt`
	
## Usage

See `bvd.py` example file

**NOTE** 

For other projects use `<your_project_name>.py` like `bs.py` for bank statement
