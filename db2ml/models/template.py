from db2ml.db import TEMPLATES
from db2ml.models.field import Field

class Template:

    def __init__(self, template: dict):
        self.data = template

    @property
    def id(self):
        #TODO: ASK Tigran for diference template[_id] and template[templare]
        return self.data["template"]

    @property
    def name(self):
        """Given the id of the template, returns the name"""
        res = TEMPLATES.find_one({"_id": self.id})
        return res["name"] if res else None

    @property
    def fields(self):
        fields = []
        for field in self.data["fields"]:
            fields.append(Field(field))
        return fields
