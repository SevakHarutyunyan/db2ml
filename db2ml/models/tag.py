

class Tag:

    def __init__(self, tag: dict):
        self.data = tag
    
    @property
    def value(self):
        return self.data["value"]
    
    @property
    def top(self):
        return self.normalize(self.data["top"])
    
    @property
    def left(self):
        return self.normalize(self.data["left"])

    @property
    def width(self):
        return self.normalize(self.data["width"])

    @property
    def height(self):
        return self.normalize(self.data["height"])

    @property
    def page(self):
        return self.data["page"]

    @staticmethod
    def normalize(percentage: str) -> float:
        return float(percentage.replace("%", "")) / 100