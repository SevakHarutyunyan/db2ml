from db2ml.models.template import Template

class Document:

    def __init__(self, document: dict):
        self.data = document

    @property
    def id(self):
        return self.data["_id"]

    @property
    def templates(self):
        templates = []
        for template in self.data["templates"]:
            templates.append(Template(template))
        return templates

    @property
    def name(self):
        return self.data["name"]
    
    @property
    def src(self):
        return self.data["src"]
    
    @property
    def pages(self):
        return self.data["pages"]
