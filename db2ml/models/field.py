from db2ml.models.tag import Tag


class Field:

    def __init__(self, field: dict):
        self.data = field

    @property
    def tags(self):
        tags = []
        for tag in self.data["tags"]:
            tags.append(Tag(tag))
        return tags
    
    @property
    def group(self):
        return self.data["group"]
    
    @property
    def name(self):
        return self.data["name"]