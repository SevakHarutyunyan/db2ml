from db2ml.db import COLLECTIONS
from db2ml.db import DOCUMENTS

from db2ml.models.document import Document

class Collection:

    def __init__(self, collection: str):
        self.data = collection
    
    @property
    def name(self):
        return self.data

    @property
    def id(self):
        """Given the name of the collection, returns the id"""
        res = COLLECTIONS.find_one({"collectionName": self.name})
        return res["_id"] if res else None
    
    @property
    def documents(self):
        documents = []
        for document in DOCUMENTS.find({'collectionId': self.id}):
            documents.append(Document(document))
        return documents
